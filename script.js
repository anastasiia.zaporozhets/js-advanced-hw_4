"use strict"

const url = "https://ajax.test-danit.com/api/swapi/films";

const div = document.createElement("div");
document.body.prepend(div);


fetch(url)
    .then(response =>{
        if(response.ok){
            return response.json();
        }
        else {
            throw new Error("Помилка завантаження фільмів")
        }
    })
    .then(data => {


        const filmPromises = data.map(({ name, episodeId, openingCrawl, characters }) => {


            const charactersPromise = characters.map(charactersUrl => {
                return fetch(charactersUrl)
                    .then(response =>{
                    if(response.ok){
                        return response.json();
                    }
                    else {
                        throw new Error("Помилка завантаження персонажа")
                    }
                })
                    .then(charactersData => charactersData.name);
            });

            return Promise.all(charactersPromise)
                .then(charactersNames => {
                    const filmContainer = document.createElement("div");
                    const filmTitle = document.createElement("h4");
                    filmTitle.textContent = `Назва фільму: ${name} Епізод: ${episodeId}`;
                    filmTitle.style.color = "red"
                    filmContainer.appendChild(filmTitle);


                    const charactersList = document.createElement("p");
                    charactersList.textContent = `Персонажі: ${charactersNames}`;
                    filmContainer.appendChild(charactersList)

                    const filmDescription = document.createElement("p");
                    filmDescription.textContent = `Опис: ${openingCrawl}`;
                    filmContainer.appendChild(filmDescription);


                    div.appendChild(filmContainer);

                });
        });

        return Promise.all(filmPromises);
    })
    .catch(error => console.error("Помилка:", error));